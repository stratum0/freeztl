#include <DallasTemperature.h>
#include <OneWire.h>

#include <PubSubClient.h>
#include <Wire.h>
#include "SSD1306.h"
#include "images.h"
SSD1306  display(0x3c, 5, 4);
#include <WiFi.h>
#include <WiFiUdp.h>
#include <NTPClient.h>


const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 0000;
const int   daylightOffset_sec = 0000;
IPAddress server(192, 168, 178, 3);
unsigned int loopcount = 127;
WiFiUDP ntpUDP;
WiFiClient client;
PubSubClient mqtt_client(client);
NTPClient timeClient(ntpUDP);
OneWire oneWire(13);
DallasTemperature sensors(&oneWire);
DeviceAddress thermometer;

int reqTemp = 0;
float potPos = 0;
float temp = 0.0;
const int wdtTimeout = 6000;
hw_timer_t *timer = NULL;


void IRAM_ATTR resetModule() {
  esp_restart();
}

String ipToString(IPAddress ip) {
  String s = "";
  for (int i = 0; i < 4; i++)
    s += i  ? "." + String(ip[i]) : String(ip[i]);
  return s;
}

void mqtt_connect() {
  mqtt_client.setServer(server, 1883);
  int loops = 0;
  while (!mqtt_client.connected()) {
    if (loops > 5) {
      break;
    }
    loops++;
    String clientId = "FreezeCTL-";
    clientId += String(random(0xffff), HEX);
    if (mqtt_client.connect(clientId.c_str())) {
      mqtt_client.publish("status/freezeCTL/heartbeat", (char*) String(timeClient.getEpochTime()).c_str());
    } else {
      delay(5000);
    }
  }
}

void setup() {
  pinMode(15, OUTPUT);
  // put your setup code here, to run once:
  timer = timerBegin(0, 80, true);                  //timer 0, div 80
  timerAttachInterrupt(timer, &resetModule, true);  //attach callback
  timerAlarmWrite(timer, wdtTimeout * 1000, false); //set time in us
  timerAlarmEnable(timer);
  digitalWrite(15, HIGH);
  display.init();
  display.flipScreenVertically();
  display.setContrast(20);
  display.drawXbm(34, 4, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  display.setTextAlignment(TEXT_ALIGN_CENTER);
  display.drawString(64, 40, "Initializing...");
  display.display();
  display.clear();
  display.drawXbm(34, 4, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  display.drawString(64, 40, "Initializing...");
  display.display();
  display.clear();
  delay(150);
  sensors.begin();
  if (! sensors.getAddress(thermometer, 0)) {
    display.drawXbm(34, 4, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
    display.drawString(64, 40, "No DS18B20 found!");
    display.display();
    display.clear();
    delay(2500);
  }
  //sensors.setResolution(thermometer, 10);
  delay(100);
  display.drawXbm(34, 4, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  display.drawString(64, 40, "Connecting...");
  display.display();
  display.clear();
  WiFi.begin("Stratum0", "stehtinderinfoecke");
  int loops = 0;
  timerWrite(timer, 0);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    if (loops > 4) {
      break;
    }
    loops++;
  }
  randomSeed(micros());
  display.drawXbm(34, 4, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  display.drawString(64, 40, "Connected!");
  display.drawString(64, 51, "IP: " + ipToString(WiFi.localIP()));
  display.display();
  display.clear();
  delay(1500);
  display.drawXbm(34, 4, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  display.drawString(64, 40, "NTP sync...");
  display.drawString(64, 51, "IP: " + ipToString(WiFi.localIP()));
  display.display();
  display.clear();
  timerWrite(timer, 0);
  timeClient.begin();
  display.drawXbm(34, 4, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  display.drawString(64, 40, "MQTT connecting...");
  display.drawString(64, 51, "IP: " + ipToString(WiFi.localIP()));
  display.display();
  display.clear();
  timerWrite(timer, 0);
  mqtt_connect();
  display.drawXbm(34, 4, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  display.drawString(64, 40, "Init done!");
  display.drawString(64, 51, "IP: " + ipToString(WiFi.localIP()));
  display.display();
  display.clear();
  delay(300);
}

void loop() {
  // put your main code here, to run repeatedly:
  timeClient.update();
  if (temp != 0) {
    timerWrite(timer, 0);
  }
  mqtt_client.loop();
  if (!mqtt_client.connected()) {
    mqtt_connect();
  }

  if (!(loopcount % 290)) {
    if (mqtt_client.connected()) {
      mqtt_client.publish("status/freezeCTL/heartbeat", (char*) String(timeClient.getEpochTime()).c_str());
    }
  }
  if (!(loopcount % 23)) {
    if (sensors.requestTemperaturesByIndex(0)) {
      float temp = sensors.getTempCByIndex(0);
      while (temp == -127) {
        sensors.requestTemperaturesByIndex(0);
        delay(100);
        temp = sensors.getTempCByIndex(0);
      }
    }
    potPos = (analogRead(36) + potPos) / 2.0;
    reqTemp = map(((int)(potPos * 10) / 10), 4095, 0, -10, -35);
    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(ArialMT_Plain_24);
    display.drawString(64, 14, String(temp, 1) + " °C");
    display.setFont(ArialMT_Plain_16);
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.drawString(0, 48, String(reqTemp) + " °C");
    display.setTextAlignment(TEXT_ALIGN_RIGHT);
    display.setFont(ArialMT_Plain_10);
    String timestr = "UTC: ";
    if (timeClient.getHours() < 10) {
      timestr += "0";
    }
    timestr += String(timeClient.getHours()) + ":";
    if (timeClient.getMinutes() < 10) {
      timestr += "0";
    }
    timestr += String(timeClient.getMinutes());
    display.drawString(128, 52, timestr);
    display.display();
    display.clear();
  }
  if (!(loopcount % 1210)) {
    if (temp < (reqTemp - 1.5)) {
      digitalWrite(15, LOW);
    } 
    if ((temp > (reqTemp + 1.8)) || temp == -127) {
      digitalWrite(15, HIGH);
    }
  }
  if (!(loopcount % 270)) {
    if (mqtt_client.connected()) {
      mqtt_client.publish("status/freezeCTL/istTemp", (char*) String(temp).c_str());
      mqtt_client.publish("status/freezeCTL/sollTemp", (char*) String(reqTemp).c_str());
    }
  }
  loopcount++;
  delay(100);

}
